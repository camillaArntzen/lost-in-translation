//sets local storage item with key 'user-session'
export function setLocalStorageItem(key, name, ttl) {
  const timeNow = new Date();

  const item = {
    name: name,
    expiry: timeNow.getTime() + ttl
  };
  localStorage.setItem(key, JSON.stringify(item));
}

//gets local storage item with key 'user-session'
export function getLocalStorageItem(key) {
  const storageItemString = localStorage.getItem(key);
  if (!storageItemString) {
    return null;
  }
  const user = JSON.parse(storageItemString);

  const timeNow = new Date();
  if (timeNow.getTime() > user.expiry) {
    localStorage.removeItem(key); //removes if expired
    return null;
  }
  return user.name;
}