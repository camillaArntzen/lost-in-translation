import React from 'react'
import './login.css'
import { createUser, checkUsername } from './usersApi.js'
import { withRouter } from 'react-router-dom'
import { useState,  useEffect, useContext } from 'react'
import { setLocalStorageItem } from "../Utils/storage"
import { AppContext } from '../AppProvider'

//bootstrap & icons
import Form from 'react-bootstrap/Form'
import Button from 'react-bootstrap/Button'
import { FaArrowCircleRight } from 'react-icons/fa'

function LoginInput({ history }){

    //global state
   const [state, dispatch] = useContext(AppContext);
   
   //local state
    const [username, setUsername] = useState('')
    

     useEffect(() => {
       if(localStorage.hasOwnProperty('user-session')){
        history.push('/translate')
       }
      }); 

    const onLoginSubmit = async (e) => {
        e.preventDefault()
       if (username !== ""){
        try {
            //checks if username already exists
            const foundUser = await checkUsername(username)

              if (foundUser) {
                const user = await checkUsername(username)
                setLocalStorageItem('user-session', username, 500000);
                history.push('/translate')
                dispatch({
                    type: "user:LOG_IN",
                    payload: [user.id, user.username, user.translations, user.favorites]
                })
                
                return
            }else{
                console.log("fant ikke")
               //creates a new user with given username
                const newUser = createUser(username)
                history.push('/translate')
                setLocalStorageItem("user-session", username, 5000000);
                dispatch({
                    type: "user:LOG_IN",
                    payload: [newUser.id, newUser.username, newUser.translations, newUser.favorites]
                })
                
            }
         
        }
        catch (error) {
            console.log(error)
        }
    }
    }

    return(
        <div className="App-LoginInput">
            <div className="login-input-container">
            <div className="button-inside">
                <form onSubmit={onLoginSubmit}>
                    <Form.Control autoFocus type="text" maxLength="30" placeholder="Whats your name?" className="input-user-name"  onChange={e => setUsername(e.target.value)} />
                    <Button type="submit" className="btn-login-input" > Log in </Button>
                    </form>
            </div>
            </div>
        </div>
    )
  
}
export default withRouter (LoginInput)