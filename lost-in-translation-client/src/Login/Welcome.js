import React from 'react'
import './welcome.css'
import logo from '../assets/Logo-Hello.png'
import Row from 'react-bootstrap/Row'
import Container from 'react-bootstrap/Container'
import Col from 'react-bootstrap/Col'
import Typewriter from 'typewriter-effect';

function Welcome(){
    return(
        <div className="App-Welcome-container">
            <div className="Welcome-container-content">
                <Container className="Welcome-container">
                    <Row>
                        <Col>
                        <img className="logo-image" src={logo} alt="logo" width="30%"/>
                        </Col>
                        <Col className="Welcome-text">
                        <h2 className="welcome-title-text"> <Typewriter
                         options={{
                            strings: ['Translate text to sign language', 'Learn sign language' ,'Get started!'],
                            autoStart: true,
                            loop: true,
                          }}
                        
                        /></h2>
                        </Col>
                    </Row>
                </Container>
            </div>
        </div>
    )
}
export default Welcome