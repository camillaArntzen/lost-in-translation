import axios from "axios";

const BASE_URL = "http://localhost:5000";

const api = axios.create({
  baseURL: BASE_URL
});

//checks if user with username exists
export function checkUsername(username) {
  return fetch(` ${BASE_URL}/users`)
      .then(response => response.json()) 
      .then(users => {
          // finding a user with username from input
          return users.find(user => user.username === username)
      })
}

//gets user with username
export function getUser(username) {
  const user = api.get("/users", {
    params: {
      username: username
    }
  })
  .then((response) => {
    return JSON.stringify(response.data[0])
  })
  .catch((e) => {
    console.log(e);
  });
  if (!user) {
    console.log("ikke user")
    return null;
  }
  
}

//creates new user with username
export function createUser(username) {
    api
      .post("/users", {
        username: username,
        translations: [],
        favorites: [],
      })
      .then((response) => {
        console.log(response)
        return JSON.stringify(response.data[0])
      })
      .catch((e) => {
        console.log(e);
      });
  }

  //deletes the user with id
  export function deleteUser(userId){
    api
    .delete("/users/"+{userId})
  }