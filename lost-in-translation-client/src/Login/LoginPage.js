import React from 'react'

import Welcome from './Welcome'
import LoginInput from './LoginInput'
import Container from '../HOC/Container'

import '../App.css'

function LoginPage(){
    return(
        <div className="App-Login-Page">
            <div className="main-content">
                <Welcome />
                <Container>
                    <LoginInput/>
                </Container>
           </div>
        </div>
    )
}
export default LoginPage