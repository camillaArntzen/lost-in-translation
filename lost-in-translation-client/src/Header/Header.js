import React from 'react'
import { useEffect, useContext } from 'react'
import './header.css'
import { AppContext } from '../AppProvider.js';
//bootstrap & icons
import Navbar from 'react-bootstrap/Navbar'
import Badge from 'react-bootstrap/Badge'
import { BsPerson } from 'react-icons/bs'
import { FaSignLanguage } from 'react-icons/fa'
import { Link } from 'react-router-dom'

function Header(){
    const [state, dispatch] = useContext(AppContext);

    return(
        <div className="App-Header">
            <Navbar className="nav">
                <Navbar.Brand href="/" className="Header-title-text"><p><FaSignLanguage/> Lost in translation</p></Navbar.Brand>
                <Navbar.Toggle />
                {/* i user is logged in --> display user name: */}
                {state.username !== null && (
                      <Navbar.Collapse className="justify-content-end">
                      <Navbar.Text>
                          <Link to="/profile"><Badge pill variant="dark" className="profile-section"><BsPerson /> {state.username}</Badge></Link>
                      </Navbar.Text>
                  </Navbar.Collapse>
                   )}
            </Navbar>
        </div>
    )
}
export default Header