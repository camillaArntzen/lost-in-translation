import './App.css'
//components
import Header from './Header/Header'
import LoginPage from './Login/LoginPage'
import NotFoundPage from './NotFound/NotFoundPage'
import TranslatePage from './Translate/TranslatePage'
import ProfilePage from './Profile/ProfilePage'

//router
import {
  BrowserRouter as Router,
  Switch,
  Route,
} from 'react-router-dom'

function App() {

  return (
    <Router>
      <div className="App">
        <Header />
        <main>
          <Switch>
            <Route exact path="/" component={ LoginPage } />
            <Route path="/translate" component = { TranslatePage } />
            <Route path="/profile" component = { ProfilePage } />
            <Route path="*" component = { NotFoundPage }/>
          </Switch>
        </main>
      </div>
    </Router>
    
  );
}

export default App
