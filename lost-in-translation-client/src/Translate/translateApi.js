import axios from "axios";

const BASE_URL = 'http://localhost:5000'

const api = axios.create({
  baseURL: BASE_URL
});
 
//updates translations array with new translation
export async function addTranslation(inTranslation, userId) {
  api
  .patch("/users/" + userId, {
    translations: inTranslation
  })
  .then((response) => {
    console.log(response);
  })
  .catch((e) => {
    console.log(e);
  });
}

export async function addFavorite(inFavorite, userId) {
  api
  .patch("/users/" + userId, {
    favorites: inFavorite,
  })
  .then((response) => {
    console.log(response);
  })
  .catch((e) => {
    console.log(e);
  });
}

//gets translations for user with username
export async function getTranslations(userId) {
  const translations =  api.get("/users", {
    params: {
      id: userId
    }
  }).then(function (response) {
    console.log(response.data[0].translations)
    return JSON.stringify(response.data[0].translations)
  })
  if (!translations) {
    return null;
  }
}

//deletes the translations for the user with userid from db.json
export function deleteTranslations(userId) {
  api.patch(`/users/`+userId, {
    translations: []
  });
}