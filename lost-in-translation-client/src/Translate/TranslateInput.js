import React from 'react'
import Form from 'react-bootstrap/Form'
import Button from 'react-bootstrap/Button'
import './translate.css'
import { useState, useContext } from "react";
import { AppContext } from '../AppProvider'
import { addTranslation, addFavorite } from './translateApi.js'
import Card from 'react-bootstrap/Card'
import Badge from 'react-bootstrap/Badge'
import Container from 'react-bootstrap/Container'
import Col from 'react-bootstrap/Col'
import Row from 'react-bootstrap/Row'
import { GrFavorite } from 'react-icons/gr'

function TranslateInput(){

    //Global state
    const [state, dispatch] = useContext(AppContext);

    const [signLanguageImages, setSignLanguageImages] = useState([]);
    const [text, setText] = useState("");
    const baseUrl = "http://localhost:5000/assets/images/"
    const regex = /^[a-z ]+$/i


    const onTranslateSubmit = (e) => {
        if (text !== ""){
            if (regex.test(text)){

        e.preventDefault();
        const images = text.split("").map((character, index) => {
            if(character === " "){
                return <span>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span>
              }
            return <img src={baseUrl+character+'.png'} alt={character} key={character + index} className="sign-image"/>
        });
        setSignLanguageImages(images);

        dispatch({ type: "translations:ADD", payload: text });
        const allTranslations = state.translations
        allTranslations.push(text)
        //save to db
        addTranslation(allTranslations, state.userId);
    }else{
        alert("You typed an invalid character. Only letters allowed!!")
    }
    }
    }; 

    function onClickFavorites(e) {
        e.preventDefault();
        
        dispatch({ type: "favorites:ADD", payload: text });
        const allFavorites = state.favorites
        allFavorites.push(text)
        //save to db
        addFavorite(allFavorites, state.userId)
      }

    return(
        <div className="translate-input">
            <Form onSubmit={onTranslateSubmit}>
                <div className="button-inside">
                <Form.Control type="text" placeholder="Type something..." className="input-user-name" maxLength="40"
                onChange={e => setText(e.target.value)} />
                <Button type="submit" className="btn-translate">Translate</Button>
                </div>
            </Form>        
            <Card className="Card">
                <Card.Body>
                    <Card.Text className="Card-Text">
                    { signLanguageImages }
                    </Card.Text>
                </Card.Body>
                <Card.Footer className="Card-footer">
                    <Container>
                    <Row>
                        <Col>
                        <Badge variant="light" className="translation-badge">Sign language translation</Badge>
                        </Col>
                        <Col >
                        <Badge variant="light" className="add-favorites-badge" onClick={onClickFavorites}><GrFavorite/> Add to favorites</Badge>
                        </Col>
                    </Row>
                    </Container>
                </Card.Footer>
            </Card>
        </div>
    )
}
export default TranslateInput