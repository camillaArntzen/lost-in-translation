import React from 'react'
import { useEffect, useContext } from 'react'
import { checkUsername } from '../Login/usersApi.js'
import { getLocalStorageItem } from '../Utils/storage.js'
import TranslateInput from './TranslateInput'
import './translate.css'
import Container from '../HOC/Container'
import withAuth from '../HOC/WithAuth.js'
import { AppContext } from '../AppProvider.js';

function TranslatePage(){

    const [state, dispatch] = useContext(AppContext);

    useEffect(() => {
        async function checkAndUpdateUser(){
          let currentUser = await getLocalStorageItem("user-session")
          let user = await checkUsername(currentUser)
          if(user){
            dispatch({
              type: "user:LOG_IN",
              payload: [user.id, user.username, user.translations, user.favorites]
            })
          } else {
          }
        }
        checkAndUpdateUser()
      },[])
  
    return(
        <div className="translate-page">
           <Container>
               <center><h2 className="translate-page-title">
                   Translate text to sign language
                   </h2></center>
               <TranslateInput />
            </Container>
        </div>
    )
}
export default withAuth (TranslatePage)