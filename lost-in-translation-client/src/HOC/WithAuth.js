import { Redirect } from 'react-router-dom'
import { getLocalStorageItem } from '../Utils/storage.js'

const withAuth = (Component) => (props) => {
  if (getLocalStorageItem('user-session') != null) {
    return <Component {...props} />;
  } else {
    return <Redirect to="/" />;
  }
}

export default withAuth;
