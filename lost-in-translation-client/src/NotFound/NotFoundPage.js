import React from 'react';
import img from '../assets/sad.png'
import '../App.css'

const NotFoundPage = () => {

    return (
        <div className="App-not-found">
            <center>
                <h1 className="not-found-title">404</h1>
                <p className="not-found-text">Page not found</p>
                <img src={img} alt="sad smiley face" width="20%"/>
            </center>
        </div>
    )

};

export default NotFoundPage;
