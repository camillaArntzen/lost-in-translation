import React from 'react'
import { createContext, useReducer } from "react";

export const AppContext = createContext();
export const ACTION_ADD_TRANSLATION = "translations:ADD";
export const ACTION_GET_USER_TRANSLATIONS = "translations:GET_ALL";
export const ACTION_DELETE_USER_TRANSLATIONS = "translations:DELETE_ALL";
export const ACTION_USER_LOG_IN = "user:LOG_IN";
export const ACTION_USER_LOG_OUT = "user:LOG_OUT";
export const ACTION_GET_USER_FAVORITES = "favorites:GET_ALL"
export const ACTION_ADD_FAVORITE = "favorites:ADD"

function appReducer(state, action) {
  switch (action.type) {
    case ACTION_GET_USER_TRANSLATIONS:
      return {
        ...state,
        translations: action.payload
      };
      case ACTION_GET_USER_FAVORITES:
      return {
        ...state,
        favorites: action.payload
      };
    case ACTION_ADD_TRANSLATION:
      return {
        ...state,
        translations: [...state.translations, action.payload]
      };
      case ACTION_ADD_FAVORITE:
        return {
          ...state,
          translations: [...state.favorites, action.payload]
        };
    case ACTION_DELETE_USER_TRANSLATIONS:
      console.log(action.payload);
      return {
        ...state,
        translations: action.payload
      };
    case ACTION_USER_LOG_IN:
      return {
        ...state,
        userId: action.payload[0],
        username: action.payload[1],
        translations: action.payload[2],
        favorites: action.payload[3]
      };
    case ACTION_USER_LOG_OUT:
      return {
        ...state,
        userId: 0,
        username: null,
        translations: [],
        favorites: [],
      };

    default:
      return state;
  }
}

const initialState = {
  userId: 0,
  username: null,
  translations: [],
  favorites: [],
};

function AppProvider(props) {
  const [state, dispatch] = useReducer(appReducer, initialState);
  
  return (
    <AppContext.Provider value={[state, dispatch]}>
      { props.children }
    </AppContext.Provider>
  );
}

export default AppProvider;