import React from 'react'
import { useEffect, useContext } from 'react'
import { Link} from 'react-router-dom'
import Button from 'react-bootstrap/Button'
import Row from 'react-bootstrap/Row'
import Col from 'react-bootstrap/Col'
import LogOutButton from './LogOutButton'
import Container from '../HOC/Container'
import TranslateFavorites from './TranslateFavorites'
import './profile.css'
import { getLocalStorageItem } from '../Utils/storage.js'
import withAuth from '../HOC/WithAuth.js'
import {
  ACTION_GET_USER_TRANSLATIONS,
  ACTION_DELETE_USER_TRANSLATIONS,
  AppContext
} from '../AppProvider.js';

import {checkUsername } from '../Login/usersApi.js'
import TranslateHistory from './TranslateHistory.js'

function ProfilePage(){
   
  //Global State
  const [state, dispatch] = useContext(AppContext);

  useEffect(() => {
    async function checkAndUpdateUser(){
      let currentUser = await getLocalStorageItem("user-session")
      console.log("provider currentUser: ", currentUser)
      let user = await checkUsername(currentUser)
      console.log("provider useEffect user: ", user)
      if(user){
        console.log("user exists")
        dispatch({
          type: "user:LOG_IN",
          payload: [user.id, user.username, user.translations, user.favorites]
        })
      } else {
        console.log("user does not exist :(")
      }
    }
    checkAndUpdateUser()
  },[])

  useEffect(() => console.log("state: ", state))

  return(
      <div className="App-ProfilePage">
          <Container>
            <h2 className="profile-title">Hello {state.username}!</h2>
            <TranslateHistory />
            <br/>
            <TranslateFavorites />
            <Row className="profile-button-section">
              <Col><LogOutButton /></Col>
              <Col><center><Link to="/translate"><Button className="back-to-translations">Back to translations</Button></Link></center></Col>
            </Row>
          </Container>
        </div>
  );
  
}
export default withAuth (ProfilePage);