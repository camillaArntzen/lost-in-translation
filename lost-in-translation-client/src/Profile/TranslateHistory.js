import React from 'react'
import {  useContext } from 'react'
import Card from 'react-bootstrap/Card'
import './profile.css'
import DeleteTranslations from './DeleteTranslations'
import { AppContext } from '../AppProvider.js';

//icon
import { FaHistory } from 'react-icons/fa'

function TranslateHistory(){
   //Global State
  const [state, dispatch] = useContext(AppContext);

  const baseUrl = 'http://localhost:5000/assets/images/'

  const listItems = () => {
    if(state.translations !== undefined){
      return [...state.translations].reverse().slice(0,10).map((text) => {
        return (
          <div className="">
            <b className="sign-text">{text}: </b>
            {
              text.split("").map((character, index) => {
                if(character === " "){
                  return <span>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span>
                } else {
                  return <img src={baseUrl+character+'.png'} alt={character} key={character + index} className="sign-images"/>
                }
              })
            }
            <hr/>
          </div>
        )
      })
    } else {
      return null
    }
  }

  return(
      <div>
            <Card className="profile-card">
              <Card.Header>
                <h2><FaHistory /> Last 10 translations:</h2>
              </Card.Header>
              <Card.Body>
                <Card.Text>
                  {listItems()}
                </Card.Text>
                <DeleteTranslations />
              </Card.Body>
            </Card>
        </div>
  );
  
}
export default TranslateHistory;