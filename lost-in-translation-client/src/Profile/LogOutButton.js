import React from 'react'
import Button from 'react-bootstrap/Button'
import { withRouter } from 'react-router-dom'
import { Redirect} from 'react-router-dom'
import { AppContext } from '../AppProvider';
import { useState, useContext} from 'react'
import './profile.css'

function LogOutButton({ history }){
    const [state, dispatch] = useContext(AppContext);

    const [redirect, setRedirect] = useState(null);

    const onLogOutClicked = async () => {
        try {
            localStorage.clear()
            dispatch({ type: "user:LOG_OUT" });
            setRedirect("/");
        }
        catch (err) {
            console.log(err);
        }
    }
    if (redirect != null) {
        return <Redirect to={redirect} />;
      }
        
    return(
        <div className="App-LogOutButton">
           <center><Button onClick={onLogOutClicked} className="logout-btn">Log out 👋 </Button></center>
        </div>
    )
}
export default withRouter (LogOutButton)
