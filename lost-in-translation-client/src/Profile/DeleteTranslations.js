import React from 'react'
import {  useContext } from 'react'
import Button from 'react-bootstrap/Button'
import './profile.css'
import {  deleteTranslations } from '../Translate/translateApi.js'
import { AppContext } from '../AppProvider.js';

//icon
import { HiOutlineTrash } from 'react-icons/hi'

function DeleteTranslations(){
   //Global State
  const [state, dispatch] = useContext(AppContext);

  function onClickDelete(e) {
    e.preventDefault();
    deleteTranslations(state.userId)
    dispatch({ type: "translations:DELETE_ALL", payload: [] });
  }
  
  return(
      <div>
          <center>
              <Button variant="danger" onClick={onClickDelete}> <HiOutlineTrash /> Clear translations</Button> 
          </center>   
        </div>
  );
  
}
export default DeleteTranslations;