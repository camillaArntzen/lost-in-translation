# Lost in translation
The third javascript assignment for the Noroff .NET upskill course winter 2021.

## Features
<p>The Lost in translation app consists of a Login page, a translation page and a profile page. The first thing the user sees is the “Login page”. The user is able to enter their name. Once the name is stored in the database the user is redirected to the translation page. On the translation page the user can write text in the input field and text will be translated once the user clicks the 'Translate' button (or presses the enter key). The user can add the translation to favorites by pressing the 'Add to Favorites' button. All of the translations is stored in the database. On the translation page the user can see their last 10 translations, their favorite translations and also clear the translations. There is also a log out button on this page. The button logs out the user and redirects back to the login page.</p>

## Setup
<ul>
<li> Clone the repository </li>
<li>

### `npm install`

</li>
<li>

### `npm start` for the server project

</li>
<li>

### `npm start` for the client project

</li>
<li>
Open [http://localhost:3000](http://localhost:3000) to view 'Lost in translation' in the browser.
</li>
</ul>

## Author
<ul>
<li>Camilla Arntzen</li>
</ul>

## Assignment requirements
### Startup / login

<p>&#10004; The first thing a user should see if the “Login page”. The user must be able to enter their name.</p>
<p>&#10004; Once the name has been stored in a node.js database, the app must display the main page, the translation page.</p>
<p>&#10004; There should be a session management solution as well. Users that are already logged in may automatically be redirected to the Translation page. You may use the browsers’ local storage to manage the session. </p>
 
### Translation
<p>&#10004; A user may only view this page if they are currently logged into the app. Please redirect a user back to the login page if now active login session exists.</p>
<p>&#10004; The user types in the input box at the top of the page. The user must click on the “translate” button to the right of the input box to trigger the translation. </p>
<p>&#10004; Translations must be stored using a node.js database solution.</p>
<p>&#10004; The Sign language characters must appear in the “translated” box.</p>

### Profile
<p>&#10004; The profile page must display the last 10 translations for the current user </p>
<p>&#10004;  There must also be a button to clear the translations. This should mark the translations as “deleted” in your database and no longer display on the profile page.</p>
<p>&#10004; The Logout button should clear all the storage and return to the start page</p>
<p>&#10004; <b>Extra functionality:</b> favorite translations</p>